jQuery(document).ready(function ($) {
  $("#contactCp").submit(function (e) {
    e.preventDefault(); // avoid to execute the actual submit of the form.

    var form = $(this);
    var url = form.attr("action");
    $(".status").empty();

    var email = $("#email").val();
    var title = $("#title").val();
    var message = $("#message").val();

    if (title == "") {
      $(".status").append(
        '<div class="alert alert-danger">Name cannot be empty</div>'
      );
      return false;
    }

    if (email == "") {
      $(".status").append(
        '<div class="alert alert-danger">Email cannot be empty</div>'
      );
      return false;
    } else {
      var re =
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (!re.test(email)) {
        $(".status").append(
          '<div class="alert alert-warning">Email format invalid</div>'
        );
        return false;
      }
    }
    if (message == "") {
      $(".status").append(
        '<div class="alert alert-danger">Message cannot be empty</div>'
      );
      return false;
    }

    // $(".status").append('<div class="alert alert-danger">Sending...</div>');

    //alert(email + ',' + title + ',' + message);
    var form = new FormData();
    form.append("email", email);
    form.append("title", title);
    form.append("message", message);

    var settings = {
      url: "https://api.staging.tradefi.ng/api/v1/support/contact_us",
      method: "POST",
      timeout: 0,
      processData: false,
      mimeType: "multipart/form-data",
      contentType: false,
      data: form,
    };

    $.ajax(settings).done(function (response) {
      // console.log(response);
      // alert(response);
      $(".status").append(
        '<div class="alert alert-success">Email successfully sent!</div>'
      );
      $("#contactCp")
        .closest("form")
        .find("input[type=text], textarea")
        .val("");
      $("#message").val("");
    });
  });
  // CONTANT END HERE

  // LOGIN STARTS HERE
  var loginform = new FormData();
  loginform.append("email", "tplus.website@comerciopartners.com");
  loginform.append("password", "wx8^nv[7J>P$cdv!");

  var user = {
    url: "https://api.staging.tradefi.ng/api/v1/token",
    method: "POST",
    timeout: 0,
    processData: false,
    mimeType: "multipart/form-data",
    contentType: false,
    data: loginform,
  };

  $.ajax(user).done(function (response) {
    var res = JSON.parse(response);
    const token = res.data.token; // token present

    //CALCULATOR START HERE

    var form = token;
    var settings = {
      url: "https://api.staging.tradefi.ng/api/v1/product_by_cat/1",
      method: "GET",
      timeout: 0,
      processData: false,
      mimeType: "multipart/form-data",
      contentType: false,
      data: "token=" + form,
    };

    $.ajax(settings).done(function (response) {
      //console.log(response);
      var obj = JSON.parse(response);
      var products1 = obj.data;
      // console.log("here", products1);

      var form = token;
      var settings = {
        url: "https://api.staging.tradefi.ng/api/v1/product_by_cat/2",
        method: "GET",
        timeout: 0,
        processData: false,
        mimeType: "multipart/form-data",
        contentType: false,
        data: "token=" + form,
      };

      $.ajax(settings).done(function (response) {
        //console.log(response);

        var obj = JSON.parse(response);
        var products2 = obj.data;
        //console.log(products2);

        var form = token;
        var settings = {
          url: "https://api.staging.tradefi.ng/api/v1/product_by_cat/3",
          method: "GET",
          timeout: 0,
          processData: false,
          mimeType: "multipart/form-data",
          contentType: false,
          data: "token=" + form,
        };

        $.ajax(settings).done(function (response) {
          //console.log(response);

          var obj = JSON.parse(response);
          var products3 = obj.data;

          var form = token;
          var settings = {
            url: "https://api.staging.tradefi.ng/api/v1/product_by_cat/4",
            method: "GET",
            timeout: 0,
            processData: false,
            mimeType: "multipart/form-data",
            contentType: false,
            data: "token=" + form,
          };

          $.ajax(settings).done(function (response) {
            var obj = JSON.parse(response);
            var products4 = obj.data;

            var products = [...products1, ...products2, ...products3, ...products4];

            //console.log(products);

            products.forEach((product) => {
              //console.log(product.clean_price);

              if (product.is_active == 1) {
                //console.log(product.is_active);
              }
              if (
                product.product_category_id == 1 ||
                product.product_category_id == 4
              ) {
                if (product.is_active == 1) {
                  //console.log('sec_name' + product.sec_name + 'discount_rate'+ product.discount_rate + 'tenor'+ product.tenor);
                  $("#marquee_api").append(
                    "<span><b> " +
                      product.sec_name +
                      " </b> " +
                      product.tenor +
                      " days " +
                      product.discount_rate.toFixed(2) +
                      '  <i style="color:#FF0000; margin-left: 6px;" class="fas fa-caret-down"></i></span>'
                  );
                }
              } else if (
                product.product_category_id == 2 ||
                product.product_category_id == 3
              ) {
                if (product.is_active == 1) {
                  //console.log(product.sec_name + product.clean_price + product.tenor);
                  $("#marquee_api").append(
                    "<span><b> " +
                      product.sec_name +
                      " </b> " +
                      product.tenor +
                      " days " +
                      product.clean_price +
                      '  <i style="color:#1ACF26; margin-left: 6px;" class="fas fa-caret-down"></i></span>'
                  );
                }
              }
            });
          });
        });
      });
    });
  });

  // var obj = JSON.parse(response);
  //   var products = obj.products;
  //   console.log(products);

  //   products.forEach(product => {

  //   console.log(product.product_temp_id)

  //   });
});

//Fetch Product By Tenor End Here

var selectedId;
var selectedProductCat;
var facevalue;

$(function () {
  $("#calulate").on("submit", function (e) {
    $("#displayPro").empty();

    e.preventDefault();

    var tenor = $("#tenor").val();
    var firstValue = $("#firstValue").val();

    if (firstValue == "" || firstValue == null) {
      document.getElementById("rq2").innerHTML = "* Required Field";
      $("#firstValue").keyup(function () {
        $("#rq2").empty();
      });
      return false;
    } else if (tenor == "" || tenor == null) {
      document.getElementById("rq").innerHTML = "* Required Field";

      $("#tenor").keyup(function () {
        $("#rq").empty();
      });
      return false;
    } else {
      if (tenor < 30) {
        document.getElementById("rq").innerHTML =
          "Tenor can not be less than 30";
        return false;
      } else {
        var tenor = $("#tenor").val();
        var firstValue = $("#firstValue").val();
        //alert(firstValue);
        var form = new FormData();
        form.append("tenor", tenor);

        var settings = {
          url: "https://api.staging.tradefi.ng/api/v1/product/calculator",
          method: "POST",
          timeout: 0,
          processData: false,
          mimeType: "multipart/form-data",
          contentType: false,
          data: form,
        };

        $.ajax(settings).done(function (response) {
          //console.log(response);

          var obj = JSON.parse(response);
          var products = obj.product;

          if (products == "" || products == null) {
            Swal.fire({
              // title: 'Error!',
              text: "No product found",
              icon: "error",
              confirmButtonText: "OK",
            });
          }

          products.forEach((product) => {
            //console.log(product.id)

            //get product categories with security id
            var productWithSec_id = [];
            if (product.product_category_id == 1) {
              var productWithSec_id = product.sec_id + " - " + "Treasury Bills";

              var product_id = product.id;
            } else {
              var productWithSec_id = product.sec_id + " - " + "FGN Bond";
              var product_id = product.id;
            }
            $("#displayPro1").empty();
            $("#displayPro").append(
              '<li class="list-group-item" data-pro_cat="' +
                product.product_category_id +
                '" data-prod_id="' +
                product.id +
                '" > <i class="fas fa-dot-circle"></i> ' +
                productWithSec_id +
                "</li>"
            );

            //console.log(productWithSec_id);
          });

          $("#displayPro li").hover(function () {
            $(this).toggleClass("active");
          });
          $("#displayPro li").click(function () {
            $(this).addClass("selected");
            var getProID = $(this).attr("data-prod_id");
            var productCat = $(this).attr("data-pro_cat");
            selectedId = getProID;
            selectedProductCat = productCat;

            //alert(firstValue);

            //Fetch Product By First Value

            //check if product categories is 1 or 2 (tbills or FGN bond)

            $("#displayPro1").empty();

            if (selectedProductCat == 1) {
              var firstValue = $("#firstValue").val();
              var settings = {
                url: "https://api.staging.tradefi.ng/api/v1/product/tbills/confirm",
                method: "POST",
                timeout: 0,
                headers: {
                  "Content-Type": "application/json",
                },
                data: JSON.stringify({
                  product_id: selectedId,
                  facevalue: firstValue,
                }),
              };

              $.ajax(settings).done(function (response) {
                //console.log(response);
                //console.log(response.summary.custody_fee);

                var str = response.summary.facevalue;
                var facevalue = +str; //simple enough and work with both int and float

                $("#displayPro1").append(
                  '<li class="list-group-item"> <i class="fas fa-dot-circle"></i> Tenor :  ' +
                    response.summary.tenor +
                    ' days </li><li class="list-group-item"> <i class="fas fa-dot-circle"></i> Value at maturity :  ' +
                    "₦" +
                    facevalue
                      .toFixed(2)
                      .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,") +
                    '</li><li class="list-group-item"> <i class="fas fa-dot-circle"></i> Cash invested  :  ' +
                    "₦" +
                    response.summary.discounted_value
                      .toFixed(2)
                      .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,") +
                    '</li><li class="list-group-item"> <i class="fas fa-dot-circle"></i> Interest receivable :  ' +
                    "₦" +
                    response.summary.interest_accrued
                      .toFixed(2)
                      .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,") +
                    "</li>"
                );
              });

              //console.log(firstValue);

              //Fetch Product By First Value End Here
            } else {
              var firstValue = $("#firstValue").val();
              var settings = {
                url: "https://api.staging.tradefi.ng/api/v1/product/fgn_bonds/confirm",
                method: "POST",
                timeout: 0,
                headers: {
                  "Content-Type": "application/json",
                },
                data: JSON.stringify({
                  product_id: selectedId,
                  facevalue: firstValue,
                }),
              };

              $.ajax(settings).done(function (response) {
                //console.log(response);
                //console.log(response.summary.custody_fee);
                var str = response.summary.facevalue;
                var facevalue = +str; //simple enough and work with both int and float

                $("#displayPro1").append(
                  '<li class="list-group-item"> <i class="fas fa-dot-circle"></i> Tenor :  ' +
                    response.summary.tenor +
                    ' days </li><li class="list-group-item"> <i class="fas fa-dot-circle"></i> Value at maturity :  ' +
                    "₦" +
                    facevalue
                      .toFixed(2)
                      .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,") +
                    '</li><li class="list-group-item"> <i class="fas fa-dot-circle"></i> Cash invested  :  ' +
                    "₦" +
                    response.summary.dirty_price
                      .toFixed(2)
                      .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,") +
                    '</li><li class="list-group-item"> <i class="fas fa-dot-circle"></i> Interest receivable :  ' +
                    "₦" +
                    response.summary.interest_accrued
                      .toFixed(2)
                      .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,") +
                    "</li>"
                );
              });
              //console.log(firstValue);

              //Fetch Product By First Value End Here
            }

            //alert(selectedProductCat);

            $(this).siblings().removeClass("selected");
          });
          //var product = JSON.parse();

          //console.log(product);
        });
        //alert(text_content);
      }
    }
  });
});

// num = 200000

// var w = '₦' + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
// console.log(w);

var current = 0;

setInterval(function () {
  var divs = $(".roll").hide();
  $(".rollout").hide();
  divs.eq(current).fadeIn("normal");
  if (current < divs.length - 1) current++;
  else current = 0;
}, 10000);

function myFunction() {
  $(document).ready(function () {
    $(".hide").hide();
    $(".show").show();
  });
}

function myFunction1() {
  $(document).ready(function () {
    $(".show").hide();
    $(".hide").show();
  });
}

function myFunction3() {
  $(document).ready(function () {
    $(".hide3").hide();
    $(".show4").show();
  });
}

function myFunction4() {
  $(document).ready(function () {
    $(".show4").hide();
    $(".hide3").show();
  });
}

function myFunction6() {
  $(document).ready(function () {
    $(".hide6").hide();
    $(".show7").show();
  });
}

function myFunction7() {
  $(document).ready(function () {
    $(".show7").hide();
    $(".hide6").show();
  });
}

function myFunction8() {
  $(document).ready(function () {
    $(".hide8").hide();
    $(".show9").show();
  });
}

function myFunction9() {
  $(document).ready(function () {
    $(".show9").hide();
    $(".hide8").show();
  });
}

$(document).ready(function () {
  var formatter = new Intl.NumberFormat("en-NG", {
    style: "currency",
    currency: "NGN",
  });

  var settings = {
    url: "https://api.staging.tradefi.ng/api/v1/app_metrics",
    method: "GET",
    timeout: 0,
  };

  $.ajax(settings).done(function (response) {
    // console.log(response.transactions_count);
    document.getElementById("app_metrics").innerHTML =
      response.data.transactions_count;
    document.getElementById("app_metrics_curt").innerHTML =
      response.data.transactions_count;

    // var amount = response.transactions_count;

    //  document.getElementById('app_metrics_curt').innerHTML = formatter.format(amount);
    // $("#app_metrics").append(response.transactions_count);

    $(".count").each(function () {
      $(this)
        .prop("Counter", 0)
        .animate(
          {
            Counter: parseFloat($(this).text().replace(/,/g, "")),
          },
          {
            duration: 10000,
            easing: "swing",
            step: function (now) {
              $(this).text(getRupeesFormat(Math.ceil(now)));
            },
          }
        );
    });

    function getRupeesFormat(val) {
      while (/(\d+)(\d{3})/.test(val.toString())) {
        val = val.toString().replace(/(\d+)(\d{3})/, "$1" + "," + "$2");
      }
      return val;
    }

    // $('.count').each(function () {
    //   $(this).prop('Counter',0).animate({
    //       Counter: $(this).text()
    //   }, {
    //       duration: 4000,
    //       easing: 'swing',
    //       step: function (now) {
    //           $(this).text(Math.ceil(now));
    //       }
    //   });
    // });
  });
});

//   $('.marquee').marquee({

// 	//duration in milliseconds of the marquee
// 	duration: 15000,
// 	//gap in pixels between the tickers
// 	gap: 50,
// 	//time in milliseconds before the marquee will start animating
// 	delayBeforeStart: 0,
// 	//'left' or 'right'
// 	direction: 'left',
// 	//true or false - should the marquee be duplicated to show an effect of continues flow
//     duplicated: true
// });

// FOR CONTACT
